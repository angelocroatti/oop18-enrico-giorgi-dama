package model;

import javax.swing.JOptionPane;
import controller.ControllerImpl;
import java.util.*;
import view.Pair;

/**
 * implementation of {@link CheckerBoardShadow} interface.
 */
public class CheckerBoardShadowImpl implements CheckerBoardShadow 
{
    /**
     *  {@link DIMENSION} dimension of the checkerBoard
     */
    public static final int DIMENSION=8;
    private List<List<PiecesType>> checkerBoard= new ArrayList<>();
    private PlayerName actualPlayer=PlayerName.playerWhite;
    private int turn=-1;
    private int eatPieceX=-1;
    private int eatPieceY=-1;
    private int setDamaX=-1;
    private int setDamaY=-1;
    private Pair<Integer, Integer> selectedItem;
    private List<Pair<Integer,Integer>> availableRedPath;
    private final ControllerImpl controller;
    private Pair<Integer, Integer> mustEatPiece;
    private int countEat=0;

    /**
     * Constructor
     */
    public CheckerBoardShadowImpl(ControllerImpl cont) 
    {

        this.controller=cont;
        this.checkerBoard= new ArrayList<>();

        for (int r=0; r<DIMENSION; r++){
            this.checkerBoard.add(new ArrayList<>());
            for (int c=0; c<DIMENSION; c++){

                this.checkerBoard.get(r).add(PiecesType.E);	


                if((r%2==0 && c%2==0)||(r%2==1 && c%2==1)) 
                {

                    if (r<3) {
                        this.checkerBoard.get(r).set(c,PiecesType.BP);
                    }
                    if(r>4 && r<8) {

                        this.checkerBoard.get(r).set(c,PiecesType.WP);
                    }

                }


            }
        }
        //the next Row instruction is a fast way to see on console the structure of the checkerBoard 
        //for (int r=0; r<DIMENSION; r++){	System.out.println(this.checkerBoard.get(r));	}
    }

    public PiecesType getType (int x, int y)
    {
        return this.checkerBoard.get(x).get(y);
    }

    public PiecesType setType (int x, int y, PiecesType pt)
    {
        return this.checkerBoard.get(x).set(y,pt);
    }

    public void selectItem(int x,int y, List<Pair<Integer,Integer>> availableRedpath )
    {
        this.selectedItem=new Pair<Integer, Integer>(x, y);
        this.availableRedPath=availableRedpath;
    }

    public boolean isSelected()
    {
        return this.selectedItem!=null;

    }

    public void unselectItem () 
    {
        this.selectedItem=null;
        if (this.availableRedPath!=null)
        {
            this.availableRedPath.clear();
        }
        this.eatPieceX=-1;
        this.eatPieceY=-1;

    }
    public List<Pair<Integer, Integer>> redPathCoordinates(List<Pair<Integer,Integer>> p) 
    {
        this.availableRedPath = new ArrayList<>(p);
        return this.availableRedPath;

    }
    public PlayerName getPlayerColor() 
    {

        return actualPlayer;

    }
    public void movePiece(int x, int y ) 
    {
        PiecesType t = this.checkerBoard.get(this.selectedItem.getX()).get(this.selectedItem.getY());
        boolean canEat=false;

        for(int i=0; i<this.availableRedPath.size();i++)
        {
            //1)eat a piece an became a Dama
            if((((this.setDamaX ==0 || this.setDamaX==7 ) && (this.setDamaX==this.availableRedPath.get(i).getX()) && (this.setDamaY==this.availableRedPath.get(i).getY()))
                    ||((this.availableRedPath.get(i).getX()==0)||(this.availableRedPath.get(i).getX()==7)))&& (this.availableRedPath.get(i).getX()==x)&&(this.availableRedPath.get(i).getY()==y))//controllo se arrivo in fondo
            {
                this.checkerBoard.get(this.selectedItem.getX()).set(this.selectedItem.getY(),PiecesType.E);
                if(this.eatPieceX != -1 && this.eatPieceY != -1)
                {
                    //set type E (empty) on the piece i'm gonna eat
                    //from the difference between the position where i'm gonna put the piece and the original one,
                    //i can understand the diagonal where is the piece i'm gonna eat and so, it's position on the checkerBoard
                    if((this.selectedItem.getX()-x==2 && this.selectedItem.getY()-y==-2)) {
                        this.checkerBoard.get(x+1).set(y-1,PiecesType.E); 
                    }
                    if((this.selectedItem.getX()-x==2 && this.selectedItem.getY()-y==2)) {
                        this.checkerBoard.get(x+1).set(y+1,PiecesType.E); 
                    }
                    if((this.selectedItem.getX()-x==-2 && this.selectedItem.getY()-y==-2)) {
                        this.checkerBoard.get(x-1).set(y-1,PiecesType.E); 
                    }
                    if((this.selectedItem.getX()-x==-2 && this.selectedItem.getY()-y==+2)) {
                        this.checkerBoard.get(x-1).set(y+1,PiecesType.E);
                    }
                    //let's set dama and change turn based on the piece selected
                    if(t==PiecesType.WP && this.actualPlayer==PlayerName.playerWhite) 
                    {
                        this.checkerBoard.get(x).set(y,PiecesType.WD);
                        this.actualPlayer=PlayerName.playerWhite;


                    }else if(t==PiecesType.BP && this.actualPlayer==PlayerName.playerBlack) 
                    {
                        this.checkerBoard.get(x).set(y,PiecesType.BD);
                        this.actualPlayer=PlayerName.playerBlack;


                    }else if(t==PiecesType.WD && this.actualPlayer==PlayerName.playerWhite) 
                    {

                        this.checkerBoard.get(x).set(y,PiecesType.WD);
                        this.actualPlayer=PlayerName.playerWhite;


                    }else if(t==PiecesType.BD && this.actualPlayer==PlayerName.playerBlack) 
                    {
                        this.checkerBoard.get(x).set(y,PiecesType.BD);
                        this.actualPlayer=PlayerName.playerBlack;


                    }
                    //can i eat again?
                    canEat=	this.controller.canIEatAgain(setDamaX, setDamaY, actualPlayer);
                    this.countEat++;

                    if (canEat && this.countEat<3) {
                        this.mustEatPiece=new Pair<Integer, Integer>(setDamaX, setDamaY);
                        this.controller.click(mustEatPiece);
                    }else {
                        if(t==PiecesType.BP || t==PiecesType.BD ) 
                        {
                            this.actualPlayer=PlayerName.playerWhite;
                        }else if(t==PiecesType.WP || t==PiecesType.WD)
                        {
                            this.actualPlayer=PlayerName.playerBlack;
                        }
                        this.mustEatPiece=null;
                        this.countEat=0;
                    }

                } 
                //2)move one position (without eat), and become a Dama
                else if(this.eatPieceX == -1 && this.eatPieceY == -1 && x==this.availableRedPath.get(i).getX() && y==this.availableRedPath.get(i).getY())
                {   
                    if(t==PiecesType.WP ) 
                    {
                        this.checkerBoard.get(x).set(y,PiecesType.WD);
                        this.actualPlayer=PlayerName.playerBlack;


                    }else if(t==PiecesType.BP) //need to avoid delete on dama on row 0 or row 7
                    {
                        this.checkerBoard.get(x).set(y,PiecesType.BD);
                        this.actualPlayer=PlayerName.playerWhite;

                    }else if(t==PiecesType.WD) 
                    {
                        this.checkerBoard.get(x).set(y,t);
                        this.actualPlayer=PlayerName.playerBlack;

                    }else if(t==PiecesType.BD) 
                    {
                        this.checkerBoard.get(x).set(y,t);
                        this.actualPlayer=PlayerName.playerWhite;

                    }
                    this.countEat=0;
                }

            }
            //3)eat a piece and don't become a Dama;
            //i'm a normal piece (black or white) or a Dama(black or white), and i'm gonna move one position without eating (i'm not in the row for becoming a dama) 
            else if((x==this.availableRedPath.get(i).getX()) && (y==this.availableRedPath.get(i).getY()) /*&& ((x!=0)||(x!=7))*/)
            {
                //i move the piece in the new position by eating
                this.checkerBoard.get(x).set(y,t);
                //set the empty type E on the piece i move
                this.checkerBoard.get(this.selectedItem.getX()).set(this.selectedItem.getY(),PiecesType.E);

                //if i eat, set empty "E" the piece i'm gonna eat
                if(this.eatPieceX != -1 && this.eatPieceY != -1) 
                {
                    if((this.selectedItem.getX()-x==2 && this.selectedItem.getY()-y==-2)) {
                        this.checkerBoard.get(x+1).set(y-1,PiecesType.E); //eat
                    }
                    if((this.selectedItem.getX()-x==2 && this.selectedItem.getY()-y==2)) {
                        this.checkerBoard.get(x+1).set(y+1,PiecesType.E); //eat
                    }
                    if((this.selectedItem.getX()-x==-2 && this.selectedItem.getY()-y==-2)) {
                        this.checkerBoard.get(x-1).set(y-1,PiecesType.E); //eat
                    }
                    if((this.selectedItem.getX()-x==-2 && this.selectedItem.getY()-y==+2)) {
                        this.checkerBoard.get(x-1).set(y+1,PiecesType.E); //eat
                    }

                    //logic of "can i eat again?" after the first eat
                    canEat=	this.controller.canIEatAgain(x, y, actualPlayer);
                    this.countEat++;

                    if (canEat && this.countEat<3) 
                    {

                        this.mustEatPiece=new Pair<Integer, Integer>(this.availableRedPath.get(i).getX(), this.availableRedPath.get(i).getY());
                        this.controller.click(mustEatPiece); 

                    }else {
                        //if i can't eat again, i'm gonna change turn to the next player 
                        if(t==PiecesType.BP || t==PiecesType.BD )
                        {
                            this.actualPlayer=PlayerName.playerWhite;

                        }else if(t==PiecesType.WP || t==PiecesType.WD) 
                        {
                            this.actualPlayer=PlayerName.playerBlack;
                        }
                        this.mustEatPiece=null;
                        this.countEat=0;
                    }


                }else {//4)move one position (without eat),and  don't become a Dama
                    this.countEat=0;
                    if(t==PiecesType.BP || t==PiecesType.BD ) 
                    {
                        this.actualPlayer=PlayerName.playerWhite;
                    }else if(t==PiecesType.WP || t==PiecesType.WD )
                    {
                        this.actualPlayer=PlayerName.playerBlack;
                    }
                }

            }
        }

        winner();
    }

    public void getEatPiece(int x, int y) 
    {
        this.eatPieceX=x;
        this.eatPieceY=y;

    }
    public Pair<Integer, Integer> getMustEatPiece()
    {
        return this.mustEatPiece;
    }

    public void setDama(int x, int y)
    {
        this.setDamaX=x;
        this.setDamaY=y;
    }	

    public int getPlayerTurn(PiecesType t)
    {
        if(PiecesType.WP==t && this.turn==-1)
        {
            turn++;
        }
        return turn;
    }

    public void winner() 
    {

        int countBlack=0;
        int countWhite=0;

        for (int r=0; r<8; r++)
        {
            for (int c=0; c<8; c++)
            {

                if(this.checkerBoard.get(r).get(c)== PiecesType.BD || this.checkerBoard.get(r).get(c)== PiecesType.BP) 
                {
                    countBlack++;
                }
                else if(this.checkerBoard.get(r).get(c)== PiecesType.WD || this.checkerBoard.get(r).get(c)== PiecesType.WP)
                {
                    countWhite++;
                }
            }

        }
        if(countWhite==0)
        {
            JOptionPane.showMessageDialog(null, "winner Black");
            this.controller.startNewGame();

        }
        if(countBlack==0 )
        {

            JOptionPane.showMessageDialog(null, "winner White");
            this.controller.startNewGame();

        }

    }

}
